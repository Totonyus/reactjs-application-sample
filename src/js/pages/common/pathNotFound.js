const React = require('react');
const {Link} = require('react-router-dom');

class pathNotFound extends React.Component {
    render() {
        return (
            <div id="pathNotFoundRootNode">
                <h3>Cette URL ne mène à rien :</h3>
                <p><code>{location.pathname + location.hash}</code></p>

                <Link to={"/"}> Retourner à l'acceuil </Link>
            </div>
        );
    }
}

module.exports = pathNotFound;