const React = require('react');
const {Link} = require('react-router-dom');

class Test2 extends React.Component {
    render() {
        console.log(this.props.match.params.id);
        return (
            <div>
                Test 2 <br/>
                <Link to={"/1"}>Test 1</Link>
            </div>);
    }
}

module.exports = Test2;