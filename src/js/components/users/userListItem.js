const React = require('react');

// props user

class UserItem extends React.Component{
    render(){
        const currentUser = this.props.user;

        return (
            <tr>
                <td>{currentUser.firstName}</td>
                <td>{currentUser.lastName}</td>
                <td>{currentUser.gender}</td>
                <td>{currentUser.town}</td>
                <td><span>Supprimer</span></td>
                <td><span>Modifier</span></td>
            </tr>
        )
    }
}

module.exports = UserItem;