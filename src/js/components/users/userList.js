const React = require('react');
const User = require('components/users/userListItem');

// props user List

class UserList extends React.Component {
    render() {
        return (
            <div id="userListContainer">
                <table>
                    <tbody>
                    {this.props.userList.map(function (user, index) {
                        return <User key={index} user={user}/>
                    })}
                    </tbody>
                </table>
            </div>);
    }
}

module.exports = UserList;