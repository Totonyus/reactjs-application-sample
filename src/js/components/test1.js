const React = require('react');
const {Link} = require('react-router-dom');

class Test1 extends React.Component {
    render() {
        const random = _.random(100);
        return (
            <div>
                Test 1 <br/>
                <Link to={"/2/" + random}>Test 2</Link>
            </div>);
    }
}

module.exports = Test1;