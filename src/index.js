const React = require('react');
const ReactDOM = require('react-dom');
const {HashRouter, Route, Switch} = require('react-router-dom');

require('css/main.scss');

ReactDOM.render(
    <HashRouter>
        <div>
            <Switch>
                <Route path="/" exact component={require('pages/common/homepage')}/>
                <Route path="/1" component={require('components/test1')}/>
                <Route path="/2/:id" component={require('components/test2')}/>

                <Route component={require('pages/common/pathNotFound')}/>
            </Switch>
        </div>
    </HashRouter>,
    document.getElementById('appRoot')
);


